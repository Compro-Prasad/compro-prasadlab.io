(function () {
  function set_style_on_mathjax_element() {
    let li_mathjax = Array.from(document.getElementsByTagName("h3"))
      .filter((x) => x.textContent.includes("Function (example 4)"))[0]
      .parentElement.getElementsByTagName("ul")[0]
      .getElementsByClassName("MathJax_Display");
    if (li_mathjax.length > 0) {
      li_mathjax[0].style.width = "auto";
      li_mathjax[0].style.setProperty("display", "inline-block", "important");
    } else {
      setTimeout(set_style_on_mathjax_element, 500);
    }
  }
  setTimeout(set_style_on_mathjax_element, 500);
})();

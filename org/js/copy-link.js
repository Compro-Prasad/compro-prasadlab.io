window.addEventListener('load', function () {
    var h2s = Array.from(document.querySelectorAll('h2[id^=h-]'));

    // Remove clicked animation from header
    // For reference: See mousedown event on h2
    document.addEventListener('mouseup', function (event) {
        setTimeout(function () {
            h2s.forEach(function (h2) {h2.classList.remove('clicked')});
        }, 103);
    });

    h2s.forEach(function (h2) {
        h2.innerHTML = '<span class="h2-title-text">' + h2.innerText + "</span>";
        var h2text = h2.querySelector("span.h2-title-text");
        var url = new URL(window.location.href);
        url.hash = '#' + h2.id;

        // If clipboard is not available show an anchor tag instead
        if (!(navigator && navigator.clipboard && navigator.clipboard.writeText)) {
            var link = document.createElement('a');
            link.href = url;
            link.innerText = url.hash;
            link.style.fontSize = '0.5em';
            h2.appendChild(link);
            return
        }

        var link = document.createElement('button');
        var copied = document.createElement('span');

        copied.textContent = 'Copied';
        copied.classList.add("copied")
        link.title = 'Copy link';
        link.innerText = '#';

        h2.appendChild(link);
        h2.appendChild(copied);

        // Copy link on clicking link
        link.addEventListener('click', function (event) {
            // First copy to clipboard
            navigator.clipboard.writeText(url);

            // Then notify the user that the URL was copied to clipboard
            var el = event.target.nextSibling;
            el.style.opacity = '1';
            el.style.visibility = 'visible';
            setTimeout(function () {
                el.style.opacity = '0';
                el.style.visibility = 'hidden';
            }, 5000);
            event.stopPropagation();
        });


        // Set clicked animation on header
        h2.addEventListener('mousedown', function (event) {
            if (event.target === h2) {
                event.target.classList.add('clicked');
            }
        });

        h2text.addEventListener('mousedown', function (event) {
            if (event.target.parentElement === h2) {
                event.target.parentElement.classList.add('clicked');
            }
        });

        h2.addEventListener('click', function (event) {
            toggleH2content(event.target);
        });

        h2text.addEventListener('click', function (event) {
            toggleH2content(event.target.parentElement);
        });

        // Toggle hide when header is clicked
        function toggleH2content(target) {
            var el = target;
            var title = target.querySelector("span.h2-title-text");
            var hidden = false;

            if (el.tagName.toLowerCase() != 'h2') {
                return;
            }

            while (el = el.nextSibling) {
                if (el.nodeType === 3) continue; // text node
                if (el.style.display == 'none') {
                    el.style.display = 'revert';
                    el.style.height = '100%';
                } else {
                    hidden = true;
                    el.style.display = 'none';
                    el.style.height = 0;
                }
            }

            if (hidden) {
                title.classList.add("collapsed");
            } else {
                title.classList.remove("collapsed");
            }
        }
    });
});

function highlightHeading () {
    if (location.hash.startsWith('#h-')) {
        var el = document.getElementById(location.hash.slice(1));
        el.classList.add('highlight-heading');
    }
}

window.addEventListener('load', function () {
    var body = document.querySelector('body');
    var title = document.querySelector('h1[class="title"]');
    if (title) {
        body.innerHTML = '<header>' + title.outerHTML + '</header>' + body.innerHTML;
        title = document.querySelectorAll('h1[class="title"]')[1];
        title.remove();
    }

    var postamble = document.querySelector('div[id="postamble"]');
    if (postamble) {
        var footer = document.createElement('footer');
        footer.id = postamble.id;
        postamble.removeAttribute('id');
        footer.innerHTML = postamble.outerHTML;
        postamble.remove();
        body.appendChild(footer);
    }

    var elements = document.querySelectorAll('[id^=h-]');
    for (var i = 0; i < elements.length; i++) {
        elements[i].ontransitionend = function (event) {
            event.target.classList.remove('highlight-heading')
        }
    }
    highlightHeading();
});

window.onhashchange = highlightHeading;

==================================================================
https://keybase.io/compro
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://compro-prasad.gitlab.io
  * I am compro (https://keybase.io/compro) on keybase.
  * I have a public key with fingerprint D796 E213 0551 20AF 20AC  6758 ACBA 2BF9 A4FF 7A01

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01201b67ef23084231cbcd41f735558c3e1463611b1132fcc8ad0b03e1cee98a82150a",
      "fingerprint": "d796e213055120af20ac6758acba2bf9a4ff7a01",
      "host": "keybase.io",
      "key_id": "acba2bf9a4ff7a01",
      "kid": "01013de8332c9e739d69dc3b61f61cc73dded854d1fcf4a0387524a4caa3500f5f790a",
      "uid": "315da70d4150154979057e2361767219",
      "username": "compro"
    },
    "merkle_root": {
      "ctime": 1642613340,
      "hash_meta": "5787dccb7d2825d327c24f1d01152718b76d2d4f0e86e6dbd2c0663eb4b84bfb",
      "seqno": 21672062
    },
    "revoke": {
      "sig_ids": [
        "11cf6e6540cfeabd5d1b9b0fb3541e9456d528daede9a2cc3773111cac2da89b0f"
      ]
    },
    "service": {
      "hostname": "compro-prasad.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1642613342,
  "expire_in": 157680000,
  "prev": "c2cb844e5dc10f14e6870095ae59b6a32cebc74d20f30229b7144442720d97ce",
  "seqno": 16,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----

owFtU29oVlUcft+1xVpay6LEIblLG0av4/w/9y7sU7CKgRTMrZy8nr/vLtve+3rv
3ZYbCxI/VCZNi1Xg+lAKKjMJwmwsGNk+NA2KiBAsVtsopX8fJigK69zVgqIDl8v9
3ed5fs/vOeeMrb8tV5ufnHrkxrGV4Xz+whWZEz8/NTbiyUjv81pHvF6z+jJ92iRp
sTfUXqsHIAJQMm4swsAnCEMllSbQckwp9RU2kDDMIJQQYmSV8oUGEriyMibwhY8g
BcIreDYsl0xcicNy6mQ1D5hBEANKXQNh3aMYp75QUiBpA0Gs5QJAR+yJkozhzEmR
mJYwcjX3UVy19z/4Nd8AYm18jJEKDMeBZoFWWDJoGVSKY62N9inR0CpLBMA+p4gI
ooTAFABLLQ9WfQ+symFIteDADU4BpCRwPyk3yA3OGUcwyICJicui3zi0ivorceSN
Frx+E/f2mWIcRWkWrUrDDAAZQQxiTIAbTyQ9xX6TCsej3OdaKck18hHVGHGFiIUa
QEgRh77kTCNNLDA+M0xLjRRgDBtJpE+klc5FYvaWI68VQWcLMOQsxGYw6jVZ9yQs
udQSr3WXB6GybOZFwygByhohNdVQBhJYiSmBJiCUaYp8LYw2gUBKYc4xdDShkBZ+
hvR2j2b94sFQrcpnG/WvALZVYpEI3VIK0z4h/9o5V04jFfU5UE+aVpLWLKV0XyVj
DRlZ/FuvKMOydifGMQZNnIRR2aXmkP8NEBU883wljE0xzBCUMx+4lfUxg5kRpFw0
xFCtILCQGOZzAAIqDA0kE+5wGKk40QhYDBAKJIfELeTC0wF3c/2TKGTOpyg5TRdj
WaQDsfFG617O4+pcvja3+f7G6m/Hf7y35uktZ7ZMlrrWbllNVXbFcnV31K9VWg7W
5i50fTNm3+k+y6pnFx76I7n8wFtnzn329tx9My2nXhq/cX2lNNd57eFLzw7rjrs+
LPx6pGfer7uydT8/276hfQ+69UFVZ9NSOvDcueZ4Z2F7/mZb1Z6Zyb4vD7xWHyxc
bR/6bnPzDx/fuqd75nItrf78+Mr3zYdOHDpdWG6M4gd/X1jZ1LGja/y9E9eeqXn8
Sfjpb7Lhq8WJdTXLEPz0CXlh//D07PWjmw4+EW3bO3/1/OLRN4+9MXLx0cVL8xuW
p458NN1Qabrz/M3d/HjbK7PlXOO6kdvNxeTV1uat0976nYenhnKdd5/Ok9cnKmC+
+92vf3ns/S/alk7Vb+S7NhYPbO84OdG0o+Hw3FLlTw==
=EEdI
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/compro

==================================================================

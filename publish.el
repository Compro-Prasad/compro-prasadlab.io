;; publish.el --- Publish website
;; Author: Abhishek(Compro) Prasad

;;; Commentary:
;; This script will convert the org-mode files to reveal js presentations and normal HTML files.

;;; Code:

(require 'package)
(package-initialize)
(require 'org)

(setq user-full-name nil)

(setq org-export-with-section-numbers nil
      org-export-with-toc nil)

(setq org-export-with-smart-quotes t)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5"
      org-html-htmlize-output-type 'css)

(setq org-publish-project-alist
      '(("orgfiles"
         :base-directory "org/"
         :base-extension "org"
         :publishing-directory "public/"
         :publishing-function org-html-publish-to-html
         ;; :exclude "PrivatePage.org" ;; regexp
         :exclude "presentations/\\|reveal.js/\\|nav-bar.org"
         :headline-levels 3
         :recursive t
         :section-numbers nil
         :with-toc nil
         ;; :html-head "<link rel=\"stylesheet\"
         ;;          href=\"../other/mystyle.css\" type=\"text/css\"/>"
         :html-preamble t)

        ("presentations"
         :base-directory "org/presentations/"
         :recursive t
         :base-extension "org"
         :publishing-directory "public/presentations/"
         :publishing-function org-re-reveal-publish-to-reveal
         :section-numbers nil
         :with-toc nil)

        ("presentation-support-files"
         :base-directory "org/presentations/"
         :base-extension "css\\|js\\|png\\|jpg\\|py\\|txt"
         :recursive t
         :publishing-directory "public/presentations/"
         :publishing-function org-publish-attachment)

        ("reveal.js"
         :base-directory "org/reveal.js/"
         :base-extension "js\\|css"
         :recursive t
         :publishing-directory "public/reveal.js/"
         :publishing-function org-publish-attachment)

        ("images"
         :base-directory "org/images/"
         :base-extension "jpg\\|jpeg\\|gif\\|png\\|svg\\|webp"
         ;; :publishing-directory "/ssh:user@host:~/html/images/"
         :publishing-directory "public/images/"
         :publishing-function org-publish-attachment)

        ("js"
         :base-directory "org/js/"
         :base-extension "js"
         :publishing-directory "public/js/"
         :publishing-function org-publish-attachment)

        ("css"
         :base-directory "org/css/"
         :base-extension "css"
         :publishing-directory "public/css/"
         :publishing-function org-publish-attachment)

        ("uploads"
         :base-directory "org/posts/"
         :base-extension "zip\\|ctf\\|png"
         :publishing-directory "public/posts/"
         :recursive t
         :publishing-function org-publish-attachment)

        ("presentation-project"
         :components ("presentations" "presentation-support-files" "reveal.js"))

        ("keybase"
         :base-directory "org/.well-known/"
         :base-extension "txt"
         :publishing-directory "public/.well-known/"
         :publishing-function org-publish-attachment)

        ("keybase2"
         :base-directory "org/.well-known/"
         :base-extension "txt"
         :publishing-directory "public/"
         :publishing-function org-publish-attachment)

        ("documents"
         :base-directory "org/documents/"
         :recursive t
         :base-extension "pdf\\|odt"
         :publishing-directory "public/documents/"
         :publishing-function org-publish-attachment)

        ("website" :components ("orgfiles" "presentation-project" "images" "js"
                                "css" "uploads" "keybase" "keybase2" "documents"))))

(provide 'publish)
;;; publish.el ends here

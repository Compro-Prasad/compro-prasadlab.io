FROM iquiw/alpine-emacs

WORKDIR src/

COPY install.el .
RUN emacs --batch --load install.el

COPY org org
COPY publish.el .

RUN emacs --batch --load publish.el --funcall org-publish-all && \
    find public/ -name '*~' -delete

FROM halverneus/static-file-server:latest
COPY --from=0 /root/src/public /web

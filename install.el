(require 'package)
(package-initialize)

(setf
 (alist-get "melpa" package-archives nil nil 'string=) "https://melpa.org/packages/"
 (alist-get "nongnu" package-archives nil nil 'string=) "https://elpa.nongnu.org/nongnu/")

(let* ((oc-p (package-installed-p 'org-contrib))
       (htz-p (package-installed-p 'htmlize))
       (ore-p (package-installed-p 'org-re-reveal))
       (all-ins-p (or oc-p htz-p ore-p)))
  (when (not all-ins-p)
    (package-refresh-contents))
  (when (not oc-p)
    (package-install 'org-contrib))
  (when (not htz-p)
    (package-install 'htmlize))
  (when (not ore-p)
    (package-install 'org-re-reveal)))

(provide 'install)
